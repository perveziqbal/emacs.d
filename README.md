# My Personal Emacs config

This configuration currently can be used to edit files using Vim keybindings,
instead of emacs keybindings.

If you are an emacs user, you could just continue using your emacs keybindings,
without any trouble, just note that if you press Ctrl+z, you will enter Vim mode,
just exit Vim mode using Ctrl+z.

Instead of using this config, consider [purcell](https://github.com/purcell/emacs.d)
if you are a beginner and would like a simpler initial configuration,
[prelude](https://github.com/bbatsov/prelude) if you need a more comprehensive configuration.

Note that the majority of code in this config, comes from purcell's config.

If you are a Vim user, open ~/.emacs.d/init.el and paste the following line at the end
of the file

    (setq evil-default-state 'normal)

If you are an emacs user and planning to switch to Vim through evil, Press Alt+x evil-tutor-start
to learn Vim. Better yet, have a look at [spacemacs](https://github.com/syl20bnr/spacemacs)

Also note that, you could easily switch between emacs and vim modes using Ctrl+z.

Emacs itself comes with support for many programming languages. This
config adds improved defaults and extended support for the following:

* Clojure (with Cider and nRepl)
* Scala (with ensime)
* Python
* Javascript
* Haskell

## Requirements

* Emacs 24.4 or greater
* To make the most of the programming language-specific support in
  this config, further programs will likely be required, particularly
  those that [flycheck](https://github.com/flycheck/flycheck) uses to
  provide on-the-fly syntax checking.

All the needed packages can be easily installed following my [mac guide](https://github.com/pervezfunctor/dotfiles/blob/master/mac-setup.md)/[linux guide](https://github.com/pervezfunctor/dotfiles/blob/master/ubuntu-vivid-setup.md)

## Installation

To install, clone this repo to `~/.emacs.d`, i.e. ensure that the
`init.el` contained in this repo ends up at `~/.emacs.d/init.el`:

```
git clone https://github.com/pervezfunctor/emacs.d.git ~/.emacs.d
```

I use [cask](http://cask.readthedocs.org/en/latest/) for installing emacs packages, so you need to do the following:

#### Ubuntu

```
curl -fsSL https://raw.githubusercontent.com/cask/cask/master/go | python
cd ~/.emacs.d && ~/.cask/bin/cask install
```

Do not forget to put `~/.cask/bin` in your PATH:
```
echo "export PATH=~/.cask/bin:\$PATH" >> ~/.zshrc # zsh
echo "export PATH=~/.cask/bin:\$PATH" >> ~/.bashrc # bash
```

#### Mac

```
brew install cask
cd ~/.emacs.d && ~/.cask/bin/cask install
```

## Important note about `ido`

This config enables `ido-mode` completion in the minibuffer wherever
possible, which might confuse you when trying to open files using
<kbd>C-x C-f</kbd>, e.g. when you want to open a directory to use
`dired` -- if you get stuck, use <kbd>C-f</kbd> to drop into the
regular `find-file` prompt. (You might want to customize the
`ido-show-dot-for-dired` variable if this is an issue for you.)

## Updates

Update the config with `cd ~/.emacs.d && cask update`. You'll probably also want/need to update
the third-party packages regularly too:

## Adding your own customization

To add your own customization, use <kbd>M-x customize</kbd> and/or
create a file `~/.emacs.d/lisp/init-local.el` which looks like this:

```el
... your code here ...

(provide 'init-local)
```

If you need initialisation code which executes earlier in the startup process,
you can also create an `~/.emacs.d/lisp/init-preload-local.el` file.

If you plan to customize things more extensively, you should probably
just fork the repo and hack away at the config to make it your own!
You also have the option of adding as many files as needed in
`~/.emacs.d/user-lisp` folder.
