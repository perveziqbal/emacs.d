(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(require 'init-essential)

(require 'init-keys)
(require 'init-evil)

(require 'init-base)

(require 'init-ui)

(require 'init-ibuffer)
(require 'init-ido)
(require 'init-helm)
(require 'init-search)

(require 'init-file)
(require 'init-windows)
(require 'init-buffer)

(require 'init-editing-utils)
(require 'init-vc)
(require 'init-ide)

(require 'init-modes)
(require 'init-lisp)
;; (require 'init-ruby-mode)
(require 'init-sql)

(require 'init-web)
(require 'init-clojure)
(require 'init-scala)
(require 'init-python)
(require 'init-haskell)

(require 'init-local nil t)

;; Locales (setting them earlier in this file doesn't work in X)
(require 'init-locales)

(when *is-a-mac*
  (require 'init-osx))

(when *is-a-linux*
  (require 'init-linux))

(require 'init-keybindings)
(require 'init-evil-keybindings)
(require 'init-hydra-keybindings)

(require 'init-spaceline)

(require 'init-misc)

(provide 'init)

;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
