(use-package clojure-mode
  :mode "\\.\\(clj\\|cljs\\)\\'"
  :init
  (defun clojure-mode-setup ()
    "Enable features useful in any Lisp mode."
    (rainbow-delimiters-mode t)
    (enable-paredit-mode)
    (aggressive-indent-mode)
    (turn-on-eldoc-mode)
    (subword-mode)
    (add-hook 'after-save-hook 'check-parens nil t))

  (after-load 'clojure-mode
    (add-hook 'clojure-mode-hook 'clojure-mode-setup)))

;; TODO: require eval-sexp-fu somewhere?
(use-package cider-eval-sexp-fu
  :defer t
  :init
  (after-load 'eval-sexp-fu
    (require 'cider-eval-sexp-fu)))

(use-package cider
  :commands (cider-jack-in cider-connect cider-jack-in-clojurescript)
  :init
  (progn
    (setq nrepl-popup-stacktraces nil)
    (add-hook 'cider-mode-hook 'eldoc-mode)
    (add-hook 'cider-repl-mode-hook 'subword-mode)
    (add-hook 'cider-repl-mode-hook 'paredit-mode)
    (setq cider-show-error-buffer nil)
    (setq cider-repl-use-clojure-font-lock t)
    ;; nrepl in't based on comint
    (add-hook 'cider-repl-mode-hook
              (lambda () (setq show-trailing-whitespace nil))))
  :config
  (define-key cider-mode-map (kbd "C-c C-d") 'ac-cider-popup-doc))

(use-package clj-refactor
  :defer t
  :diminish clj-refactor-mode
  :init
  (add-hook 'clojure-mode-hook 'clj-refactor-mode)
  :config
  (progn
    (cljr-add-keybindings-with-prefix "C-c C-f")))

(use-package yesql-ghosts
  :commands (yesql-ghosts-auto-show-ghosts)
  :init
  (add-hook 'cider-mode-hook 'yesql-ghosts-auto-show-ghosts))

(use-package 4clojure
  :commands (4clojure-open-question))

(use-package clojure-cheatsheet
  :commands (clojure-cheatsheet))

(use-package cljsbuild-mode
  :commands (cljsbuild-start))

(provide 'init-clojure)
