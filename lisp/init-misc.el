(use-package paradox
  :commands (paradox-list-packages))

(use-package helm-mode-manager
  :commands (helm-switch-major-mode helm-enable-minor-mode helm-disable-minor-mode))

(use-package esup
  :commands (esup))

(provide 'init-misc)
