(use-package haskell-mode
  :defer t)

(use-package ac-haskell-process
  :defer t
  :init
  (progn
    (add-hook 'interactive-haskell-mode-hook 'ac-haskell-process-setup)
    (add-hook 'haskell-interactive-mode-hook 'ac-haskell-process-setup)
    (after-load 'haskell-mode
      (define-key haskell-mode-map (kbd "C-c C-d") 'ac-haskell-process-popup-doc))))

(after-load 'auto-complete
  (add-to-list 'ac-modes 'haskell-interactive-mode)
  (add-hook 'haskell-interactive-mode-hook 'set-auto-complete-as-completion-at-point-function))

(after-load 'auto-complete
  (add-to-list 'ac-modes 'haskell-interactive-mode)
  (add-hook 'haskell-interactive-mode-hook 'set-auto-complete-as-completion-at-point-function))

(when (executable-find "ghci-ng")
  (setq-default haskell-process-args-cabal-repl
                '("--ghc-option=-ferror-spans" "--with-ghc=ghci-ng")))

(use-package flycheck-haskell
  :defer t)

(after-load 'flycheck
  (add-hook 'haskell-mode-hook #'flycheck-haskell-setup)

  (defun flycheck-haskell-reconfigure ()
    "Reconfigure flycheck haskell settings, e.g. after changing cabal file."
    (interactive)
    (unless (eq major-mode 'haskell-mode)
      (error "Expected to be in haskell-mode"))
    (flycheck-haskell-clear-config-cache)
    (flycheck-haskell-configure)
    (flycheck-mode -1)
    (flycheck-mode))
  (require 'flycheck-hdevtools))

(dolist (hook '(haskell-mode-hook inferior-haskell-mode-hook haskell-interactive-mode-hook))
  (add-hook hook 'turn-on-haskell-doc-mode)
  (add-hook hook (lambda () (subword-mode +1)))
  (add-hook hook (lambda () (eldoc-mode 1))))
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)

(add-hook 'haskell-interactive-mode-hook
          (lambda () (setq show-trailing-whitespace nil)))

(after-load 'haskell
  (diminish 'interactive-haskell-mode " IntHS"))

(add-auto-mode 'haskell-mode "\\.ghci\\'")

(use-package ghci-completion)
(add-hook 'inferior-haskell-mode-hook 'turn-on-ghci-completion)

(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(add-hook 'haskell-mode-hook 'haskell-auto-insert-module-template)
(setq-default haskell-stylish-on-save t)

(use-package hayoo)
(after-load 'haskell-mode
  (define-key haskell-mode-map (kbd "C-c h") 'hoogle)
  (define-key haskell-mode-map (kbd "C-o") 'open-line))

(after-load 'page-break-lines
  (push 'haskell-mode page-break-lines-modes))

;; Make compilation-mode understand "at blah.hs:11:34-50" lines output by GHC
(after-load 'compile
  (let ((alias 'ghc-at-regexp))
    (add-to-list
     'compilation-error-regexp-alist-alist
     (list alias " at \\(.*\\.\\(?:l?[gh]hs\\|hi\\)\\):\\([0-9]+\\):\\([0-9]+\\)-[0-9]+$" 1 2 3 0 1))
    (add-to-list
     'compilation-error-regexp-alist alias)))

;; Stop haskell-mode's compiler note navigation from clobbering highlight-symbol-nav-mode
(after-load 'haskell
  (define-key interactive-haskell-mode-map (kbd "M-n") nil)
  (define-key interactive-haskell-mode-map (kbd "M-p") nil)
  (define-key interactive-haskell-mode-map (kbd "M-N") 'haskell-goto-next-error)
  (define-key interactive-haskell-mode-map (kbd "M-P") 'haskell-goto-prev-error))

(use-package idris-mode
  :defer t)

(use-package elm-mode
  :defer t
  :init
  (after-load 'auto-complete
    (add-to-list 'ac-modes 'elm-mode))
  (after-load 'elm-mode
    (when (executable-find 'elm-oracle)
      (add-hook 'elm-mode-hook 'elm-oracle-setup-ac)))
  (progn
    (if (fboundp 'electric-indent-local-mode)
        (electric-indent-local-mode -1))
    ;; (add-hook 'elm-mode-hook #'smartparens-strict-mode)
    ))

(use-package flycheck-elm
  :defer t
  :init
  (after-load 'elm-mode
    (flycheck-elm-setup)))

(use-package purescript-mode
  :defer t
  :init
  (add-hook 'purescript-mode-hook 'turn-on-purescript-indentation))

(use-package psci
  :defer t
  :init
  (progn
    (add-hook 'purescript-mode-hook 'inferior-psci-mode)))

(provide 'init-haskell)
