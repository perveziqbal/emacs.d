(use-package evil-escape
  :diminish evil-escape-mode
  :init
  (evil-escape-mode))

;; 2-char searching ala vim-sneak & vim-seek, for evil-mode and Emacs
(use-package evil-snipe
  :diminish evil-snipe-local-mode
  :config
  (evil-snipe-mode)
  (evil-snipe-override-mode))

;; you will be surrounded (surround.vim for evil, the extensible vi layer)
(use-package evil-surround
  :config
  (global-evil-surround-mode)
  (evil-define-key 'visual evil-surround-mode-map "s" 'evil-surround-region)
  (evil-define-key 'visual evil-surround-mode-map "S" 'evil-substitute))

(use-package evil-visualstar
  :commands (evil-visualstar/begin-search-forward
             evil-visualstar/begin-search-backward)
  :init
  (global-evil-visualstar-mode)
  (define-key evil-visual-state-map (kbd "*")
    'evil-visualstar/begin-search-forward)
  (define-key evil-visual-state-map (kbd "#")
    'evil-visualstar/begin-search-backward))

;; Motions and text objects for delimited arguments in Evil.
(use-package evil-args
  :config
  (progn
    ;; bind evil-args text objects
    (bind-key "a" 'evil-inner-arg evil-inner-text-objects-map)
    (bind-key "a" 'evil-outer-arg evil-outer-text-objects-map)

    ;; bind evil-forward/backward-args
    (bind-key "L" 'evil-forward-arg evil-normal-state-map)
    (bind-key "H" 'evil-backward-arg evil-normal-state-map)
    (bind-key "L" 'evil-forward-arg evil-motion-state-map)
    (bind-key "H" 'evil-backward-arg evil-motion-state-map)))

;; Start a * or # search from the visual selection
(use-package evil-matchit
  :config
  (global-evil-matchit-mode))

(use-package evil-iedit-state
  :config
  (evil-leader/set-key "se" 'evil-iedit-state/iedit-mode))

(use-package evil-indent-textobject)

(use-package evil-jumper
  :init
  (setq evil-jumper-file (concat user-emacs-directory ".evil-jumps")
        evil-jumper-auto-save-interval 3600)

  :config
  (evil-jumper-mode))

(use-package evil-nerd-commenter
  :init
  (progn
    (evil-leader/set-key
      ";"  'evilnc-comment-operator
      "cl" 'evilnc-comment-or-uncomment-lines
      "ci" 'evilnc-toggle-invert-comment-line-by-line
      "cp" 'evilnc-comment-or-uncomment-paragraphs
      "ct" 'evilnc-quick-comment-or-uncomment-to-the-line
      "cy" 'evilnc-copy-and-comment-lines)))


(use-package linum-relative
  :commands linum-relative-toggle
  :init
  (evil-leader/set-key "tr" 'linum-relative-toggle)
  :config
  (progn
    (setq linum-format 'linum-relative)
    (setq linum-relative-current-symbol "")
    (linum-relative-toggle)))

(use-package evil-anzu
  :diminish anzu-mode
  :init
  (global-anzu-mode)
  :config
  (setq anzu-search-threshold 1000
        anzu-cons-mode-line-p nil))

(use-package evil-tutor)

(provide 'init-evil)
