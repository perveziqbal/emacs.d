(require 'solarized)

(defun light ()
  "Activate a light color theme."
  (interactive)
  (load-theme 'solarized-light nil nil))

(defun dark ()
  "Activate a dark color theme."
  (interactive)
  (load-theme 'monokai nil nil))

(defun space-dark ()
  "Activate spacemacs dark theme"
  (interactive)
  (load-theme 'spacemacs-dark))

(defun space-light ()
  "Activate spacemacs dark theme"
  (interactive)
  (load-theme 'spacemacs-light))

(defun leuven ()
  (interactive)
  (load-theme 'leuven nil nil))

(defun sol-dark ()
  (interactive)
  (load-theme 'solarized-dark nil nil))

(defun sol-light ()
  (interactive)
  (load-theme 'solarized-light nil nil))

(defadvice load-theme (before theme-dont-propagate activate)
  (mapcar #'disable-theme custom-enabled-themes))

(provide 'init-ui)
