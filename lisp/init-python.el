;;python
(use-package anaconda-mode
  :defer t
  :init
  (progn
    (add-hook 'python-mode-hook 'anaconda-mode)
    (add-hook 'python-mode-hook 'eldoc-mode)))

(use-package ac-anaconda
  :defer t
  :config
  (add-hook 'python-mode-hook 'ac-anaconda-setup))

(use-package ein
  :commands (ein:notebooklist-open))

(use-package pyenv-mode
  :defer t
  :config
  (add-hook 'python-mode-hook 'pyenv-mode))

(defun projectile-pyenv-mode-set ()
  "Set pyenv version matching project name.
Version must be already installed."
  (pyenv-mode-set (projectile-project-name)))
(add-hook 'projectile-switch-project-hook 'projectile-pyenv-mode-set)

(use-package cython-mode :defer t)

(setq auto-mode-alist
      (append '(("SConstruct\\'" . python-mode)
                ("SConscript\\'" . python-mode))
              auto-mode-alist))

;; (use-package company-anaconda
;;   :init
;;   (progn
;;     (with-eval-after-load "company"
;;       (add-to-list 'company-backends 'company-anaconda))
;;     (add-hook 'python-mode-hook 'anaconda-mode)))

(use-package pip-requirements
  :defer t)

(provide 'init-python)
