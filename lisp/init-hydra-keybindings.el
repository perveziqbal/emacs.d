(defhydra hydra-window-transparency ()
  "window-transparency"
  ("-" (adjust-opacity nil -2))
  ("+" (adjust-opacity nil 2))
  ("0" (modify-frame-parameters nil `((alpha . 100))))
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-move-dup ()
  "move-dup"
  ("k" md/move-lines-up)
  ("j" md/move-lines-down)
  ("K" md/duplicate-up)
  ("J" md/duplicate-down)
  ("<ESC>" nil)
  ("q" nil))

(defhydra hydra-font-zoom ()
  "font-zoom"
  ("+" text-scale-increase "in")
  ("-" text-scale-decrease "out")
  ("=" (text-scale-set 0))
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-splitter ()
  "splitter"
  ("h" hydra-move-splitter-left)
  ("j" hydra-move-splitter-down)
  ("k" hydra-move-splitter-up)
  ("l" hydra-move-splitter-right)
  ("b" balance-windows)
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-movement ()
  "movement"
  ("n" next-line "next")
  ("p" previous-line "prev")
  ("u" scroll-up "up")
  ("d" scroll-down "down")
  ("r" scroll-right "right")
  ("l" scroll-left "left")
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-jump (:color blue)
  "jump"
  ("c" ace-jump-char-mode "char")
  ("w" ace-jump-word-mode "word")
  ("l" ace-jump-line-mode "line")
  ("g" goto-line "line no")
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-error ()
  "goto-error"
  ("f" first-error "first")
  ("n" next-error "next")
  ("p" previous-error "prev")
  ("v" recenter-top-bottom "recenter")
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-other-window ()
  "other-window"
  ("n" (scroll-other-window 1) "next line")
  ("p" (scroll-other-window -1) "prev line")
  ("N" scroll-other-window "next page")
  ("P" (scroll-other-window '-) "prev page")
  ("a" (beginning-of-buffer-other-window 0) "begin")
  ("e" (end-of-buffer-other-window 0) "end")
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-toggle (:color blue)
  "toggle"
  ("i" indent-guide-mode "indent guide")
  ("s" smartparens-strict-mode "smartparenss")
  ("n" neotree-toggle "neotree")
  ("t" multi-term-dedicated-toggle "multi term")
  ("g" golden-ratio-mode "golden ratio")
  ("h" global-hl-line-mode "highlight line")
  ("l" linum-mode)
  ("f" toggle-frame-fullscreen "fullscreen")
  ("m" toggle-frame-maximized "mazimized")
  ("d" toggle-debug-on-error "debug")
  ("w" whitespace-mode "whitespace")
  ("<ESC>" nil "quit")
  ("q" nil "quit"))

(defhydra hydra-yank-pop ()
  "yank"
  ("C-y" yank nil)
  ("M-y" yank-pop nil)
  ("y" (yank-pop 1) "next")
  ("Y" (yank-pop -1) "prev")
  ("l" browse-kill-ring "list" :color blue)
  ("q" nil "cancel"))

(use-package git-gutter)

(defhydra hydra-git-gutter (:body-pre (git-gutter-mode 1))
  ("j" git-gutter:next-hunk "next")
  ("k" git-gutter:previous-hunk "prev")
  ("h" (progn (goto-char (point-min))
              (git-gutter:next-hunk 1)) "first")
  ("l" (progn (goto-char (point-min))
              (git-gutter:previous-hunk 1)) "last")
  ("s" git-gutter:stage-hunk "stage")
  ("r" git-gutter:revert-hunk "revert")
  ("p" git-gutter:popup-hunk "pop")
  ;;  ("R" git-gutter:set-start-revision)
  ("q" nil :color blue)
  ("Q" (progn (git-gutter-mode -1)
              ;; git-gutter-fringe doesn't seem to
              ;; clear the markup right away
              (sit-for 0.1)
              (git-gutter:clear))
   :color blue))

(defhydra hydra-projectile (:color teal)
  ("a"   projectile-ag "ag")
  ("b"   projectile-switch-to-buffer "switch buffer")
  ("c"   projectile-invalidate-cache "clear cache")
  ("d"   projectile-find-dir "dir")
  ("s-f" projectile-find-file "find file")
  ("ff"  projectile-find-file-dwim "file dwim")
  ("fd"  projectile-find-file-in-directory "file in dir")
  ("i"   projectile-ibuffer)
  ("K"   projectile-kill-buffers "Kill")
  ("o"   projectile-multi-occur "multi occur")
  ("s"   projectile-switch-project "switch project")
  ("r"   projectile-recentf "recentf")
  ("x"   projectile-remove-known-project "remove")
  ("X"   projectile-cleanup-known-projects "cleanup")
  ("z"   projectile-cache-current-file "cache current")
  ("`"   hydra-projectile-other-window/body "other window")

  ("F"  projectile-find-file-other-window        "file")
  ("G"  projectile-find-file-dwim-other-window   "file dwim")
  ("D"  projectile-find-dir-other-window         "dir")
  ("B"  projectile-switch-to-buffer-other-window "buffer")

  ("q"   nil "cancel" :color blue))

(defhydra hydra-page (ctl-x-map "" :pre (widen))
  "page"
  ("]" forward-page "next")
  ("[" backward-page "prev")
  ("n" narrow-to-page "narrow" :bind nil :exit t))

(defhydra hyrda-helm-search (:color blue)
  "helm-search"
  ("a" helm-ag)
  ("f" helm-do-ag-this-file)
  ("s" helm-swoop)
  ("b" helm-do-ag-buffers)
  ("p" helm-do-ag-project-root)
  ("q" nil "cancel"))

(bind-key "C-, z" 'hydra-zoom/body)
(bind-key "C-, m" 'hydra-movement/body)
(bind-key "C-, j" 'hydra-jump/body)
(bind-key "C-, o" 'hydra-other-window/body)
(bind-key "C-, t" 'hydra-toggle/body)
(bind-key "C-, e" 'hydra-error/body)
(bind-key "C-, s" 'hydra-splitter/body)
(bind-key "C-, p" 'hydra-projectile/body)
(bind-key "C-, P" 'hydra-page/body)
(bind-key "C-, g" 'hydra-git-gutter/body)

(bind-key "M-y" 'hydra-yank-pop/yank-pop)
(bind-key "C-y" 'hydra-yank-pop/yank)

(bind-key "C-, l" 'hydra-move-dup/body)

(bind-key "C-, z" 'hydra-font-zoom/body)
(bind-key "C-, t" 'hydra-toggle/body)
(bind-key "C-, e" 'hydra-error/body)
(bind-key "C-, s" 'hydra-splitter/body)
(bind-key "C-, T" 'hydra-window-transparency/body)


;; acejump? multiple-cursors?

(provide 'init-hydra-keybindings)
