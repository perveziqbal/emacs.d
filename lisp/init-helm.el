(use-package helm
  :diminish helm-mode
  :defer t
  :bind ( ("C-c h x" . helm-M-x)
          ("C-c h y" . helm-show-kill-ring)
          ("C-c h b" . helm-mini)
          ("C-c h f" . helm-find-files)
          ("C-c h s" . helm-ff-run-grep)
          ("C-c h i" . helm-semantic-or-imenu)
          ("C-c h m" . helm-man-woman)
          ("C-c h /" . helm-find)
          ("C-c h l" . helm-locate)
          ("C-c h o" . helm-occur)
          ("C-c h a" . helm-apropos)

          ("C-c h h g" . helm-info-gnus)
          ("C-c h h i" . helm-info-at-point)
          ("C-c h h r" . helm-info-emacs)
          ("C-c h <tab>" . helm-lisp-completion-at-point)

          ("C-c h B" . helm-resume)
          ("C-h SPC" . helm-all-mark-rings)
          ("C-c h r" . helm-regex)
          ("C-c h X" . helm-register)
          ("C-c h t" . helm-top)
          ("C-c h g" . helm-google-suggest)
          ("C-c h M-:" . helm-eval-expression-with-eldoc)
          ("C-c C-l" . helm-eshell-history)
          ("C-c C-l" . helm-comint-input-ring)
          ("C-c C-l" . helm-mini-buffer-history))
  :config
  (progn
    (helm-autoresize-mode 1)

    ;; from https://www.reddit.com/r/emacs/comments/2z7nbv/lean_helm_window/
    (defvar helm-source-header-default-background (face-attribute 'helm-source-header :background))
    (defvar helm-source-header-default-foreground (face-attribute 'helm-source-header :foreground))
    (defvar helm-source-header-default-box (face-attribute 'helm-source-header :box))
    (defvar helm-source-header-default-height (face-attribute 'helm-source-header :height)))
  :init
  (progn
    (setq helm-prevent-escaping-from-minibuffer t
          helm-bookmark-show-location t
          helm-display-header-line nil
          helm-split-window-in-side-p t
          helm-always-two-windows t
          helm-echo-input-in-header-line t
          helm-imenu-execute-action-at-once-if-one nil)

    ;; fuzzy matching setting
    (setq helm-M-x-fuzzy-match t
          helm-apropos-fuzzy-match t
          helm-file-cache-fuzzy-match t
          helm-imenu-fuzzy-match t
          helm-lisp-fuzzy-completion t
          helm-recentf-fuzzy-match t
          helm-semantic-fuzzy-match t
          helm-buffers-fuzzy-matching t)

    ;; helm-locate uses es (from everything on windows, which doesnt like fuzzy)
    (setq helm-locate-fuzzy-match (executable-find "locate"))

    (evil-leader/set-key
      "<f1>" 'helm-apropos
      "bb"   'helm-mini
      "Cl"   'helm-colors
      "ff"   'helm-find-files
      "fL"   'helm-locate
      "fr"   'helm-recentf
      "hb"   'helm-filtered-bookmarks
      "hi"   'helm-info-at-point
      "hl"   'helm-resume
      "hm"   'helm-man-woman
      "ry"   'helm-show-kill-ring
      "rr"   'helm-register
      "rm"   'helm-all-mark-rings)

    ;; Add minibuffer history with `helm-minibuffer-history'
    (define-key minibuffer-local-map (kbd "C-c C-l") 'helm-minibuffer-history))
  :config
  (progn
    (helm-mode +1)
    ;; alter helm-bookmark key bindings to be simpler
    (defun simpler-helm-bookmark-keybindings ()
      (define-key helm-bookmark-map (kbd "C-d") 'helm-bookmark-run-delete)
      (define-key helm-bookmark-map (kbd "C-e") 'helm-bookmark-run-edit)
      (define-key helm-bookmark-map (kbd "C-f") 'helm-bookmark-toggle-filename)
      (define-key helm-bookmark-map (kbd "C-o") 'helm-bookmark-run-jump-other-window)
      (define-key helm-bookmark-map (kbd "C-/") 'helm-bookmark-help))
    (add-hook 'helm-mode-hook 'simpler-helm-bookmark-keybindings)

    ;; Swap default TAB and C-z commands.
    ;; For GUI.
    (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
    ;; For terminal.
    (define-key helm-map (kbd "TAB") 'helm-execute-persistent-action)
    (define-key helm-map (kbd "C-z") 'helm-select-action)))

(use-package helm-swoop
  :bind
  (("C-S-s" . helm-swoop)
   ("M-i" . helm-swoop)
   ("M-s s" . helm-swoop)
   ("M-s M-s" . helm-swoop)
   ("M-I" . helm-swoop-back-to-last-point)
   ("C-c M-i" . helm-multi-swoop)
   ("C-x M-i" . helm-multi-swoop-all)
   )
  :config
  (progn
    (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)
    (define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)))

(provide 'init-helm)
