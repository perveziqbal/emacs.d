(setq mac-option-modifier 'meta)
(setq ns-function-modifier 'hyper)  ; make Fn key do Hyper

(when (fboundp 'toggle-frame-fullscreen)
  ;; Command-Option-f to toggle fullscreen mode
  ;; Hint: Customize `ns-use-native-fullscreen'
  (global-set-key (kbd "M-ƒ") 'toggle-frame-fullscreen))

(provide 'init-osx)
