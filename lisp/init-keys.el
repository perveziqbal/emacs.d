(use-package guide-key
  :diminish guide-key-mode
  :config
  (progn
    (setq guide-key/guide-key-sequence '("C-x" "C-c" "C-," "M-SPC" "M-SPC m"))
    (setq guide-key/recursive-key-sequence-flag t)
    (setq guide-key/popup-window-position 'bottom)
    (guide-key-mode)))

(use-package helm-descbinds
  :defer t
  :bind (("C-h b" . helm-descbinds)))

(use-package key-chord
  :defer t
  :config
  (key-chord-mode +1))

(use-package hydra
  :defer t
  :config
  (require 'hydra-examples))

(use-package evil
  :defer t
  :init
  (setq evil-default-state 'emacs)

  (setq evil-normal-state-cursor '("DarkGoldenrod2" box))
  (setq evil-insert-state-cursor '("chartreuse3" (bar . 2)))
  (setq evil-emacs-state-cursor '("SkyBlue2" box))
  (setq evil-replace-state-cursor '("chocolate" (hbar . 2)))
  (setq evil-visual-state-cursor '("gray" (hbar . 2)))
  (setq evil-motion-state-cursor '("plum3" hollow))

  :config
  (progn
    (setq evil-emacs-state-modes (append evil-insert-state-modes evil-emacs-state-modes))
    (setq evil-insert-state-modes nil)

    (setq evil-emacs-state-modes (append evil-motion-state-modes evil-emacs-state-modes))
    (setq evil-motion-state-modes nil)

    (evil-mode)))

(use-package evil-leader
  :defer t
  :config
  (progn
    (global-evil-leader-mode)
    (setq evil-leader/in-all-states t)
    (setq evil-leader/non-normal-prefix "M-")
    (define-key key-translation-map (kbd "M-m") (kbd "M-SPC"))
    (define-key key-translation-map (kbd "M-RET") (kbd "M-SPC m"))
    (evil-leader/set-leader "<SPC>")))

(provide 'init-keys)
