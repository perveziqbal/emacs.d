;; spaceline should be defined after all it's dependencies

(use-package spaceline
  :config
  (progn
    (defadvice load-theme (after reset-powerline-after-load-theme activate)
      (powerline-reset))

    (setq-default spaceline-window-numbers-unicode t)
    (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
    (require 'spaceline-config)
    (spaceline-install
     '(((workspace-number window-number)
        :fallback evil-state
        :separator "|"
        :face highlight-face)
       ((point-position
         line-column)
        :separator " | ")
       (buffer-modified buffer-id remote-host)
       anzu
       major-mode
       ((flycheck-error flycheck-warning flycheck-info)
        :when active)
       (((minor-modes :separator spaceline-minor-modes-separator)
         process)
        :when active)
       (version-control :when active))

     `(selection-info
       (global :when active)
       buffer-position))))

(provide 'init-spaceline)
