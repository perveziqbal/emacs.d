(use-package csv-mode
  :mode "\\.[Cc][Ss][Vv]\\'")

(use-package csv-nav)
(setq csv-separators '("," ";" "|" " "))

(use-package markdown-mode
  :mode "\\.\\(md\\|markdown\\)\\'")

(provide 'init-modes)
