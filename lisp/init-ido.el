;; Use C-f during file selection to switch to regular find-file

(use-package ido
  :init
  (progn
    (customize-set-variable 'ido-save-directory-list-file
                            (concat seartipy-cache-directory "ido-last"))
    (setq ido-enable-flex-matching t)
    (setq ido-use-filename-at-point nil)
    (setq ido-auto-merge-work-directories-length 0)
    (setq ido-use-virtual-buffers t)
    (setq ido-create-new-buffer 'always))
  :config
  (progn
    (ido-mode)
    (ido-everywhere)))

(use-package ido-ubiquitous
  :config
  (ido-ubiquitous-mode))

;; Use smex to handle M-x
(use-package smex
  :init
  ;; Change path for ~/.smex-items
  (setq smex-save-file (expand-file-name ".smex-items" seartipy-cache-directory))

  :config
  (bind-key [remap execute-extended-command] 'smex))

(use-package idomenu
  :init
  (progn
    ;; Allow the same buffer to be open in different frames
    (setq ido-default-buffer-method 'selected-window)
    ;; http://www.reddit.com/r/emacs/comments/21a4p9/use_recentf_and_ido_together/cgbprem
    (add-hook 'ido-setup-hook (lambda () (bind-key  [up] 'previous-history-element ido-completion-map)))))

(use-package flx-ido
  :config
  (flx-ido-mode))

(use-package ido-vertical-mode
  :config
  (ido-vertical-mode))

(provide 'init-ido)
