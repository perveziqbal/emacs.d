(use-package eldoc
  :diminish eldoc-mode
  :defer t
  :config
  (progn
    ;; enable eldoc in `eval-expression'
    (add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode)))



;;;; Compile

(setq-default compilation-scroll-output t)

(use-package alert
  :commands (alert))

;; Customize `alert-default-style' to get messages after compilation

(defun alert-after-compilation-finish (buf result)
  "Use `alert' to report compilation RESULT if BUF is hidden."
  (unless (catch 'is-visible
            (walk-windows (lambda (w)
                            (when (eq (window-buffer w) buf)
                              (throw 'is-visible t))))
            nil)
    (alert (concat "Compilation " result)
           :buffer buf
           :category 'compilation)))

(after-load 'compile
  (add-hook 'compilation-finish-functions
            'alert-after-compilation-finish))

(defvar last-compilation-buffer nil
  "The last buffer in which compilation took place.")

(after-load 'compile
  (defadvice compilation-start (after save-compilation-buffer activate)
    "Save the compilation buffer to find it later."
    (setq last-compilation-buffer next-error-last-buffer))

  (defadvice recompile (around find-prev-compilation (&optional edit-command) activate)
    "Find the previous compilation buffer, if present, and recompile there."
    (if (and (null edit-command)
             (not (derived-mode-p 'compilation-mode))
             last-compilation-buffer
             (buffer-live-p (get-buffer last-compilation-buffer)))
        (with-current-buffer last-compilation-buffer
          ad-do-it)
      ad-do-it)))

(bind-key [f6] 'recompile)

(defadvice shell-command-on-region
    (after shell-command-in-view-mode
           (start end command &optional output-buffer replace error-buffer display-error-buffer)
           activate)
  "Put \"*Shell Command Output*\" buffers into view-mode."
  (unless output-buffer
    (with-current-buffer "*Shell Command Output*"
      (view-mode 1))))

(after-load 'compile
  (require 'ansi-color)
  (defun colourise-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  (add-hook 'compilation-filter-hook 'colourise-compilation-buffer))



(use-package flycheck
  :defer t
  :diminish flycheck-mode
  :init
  (progn
    (add-hook 'after-init-hook 'global-flycheck-mode)
    (setq flycheck-check-syntax-automatically '(save new-line mode-enabled)
          flycheck-idle-change-delay 0.8
          flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-list)
    (after-load 'flycheck
      '(custom-set-variables
        '(flycheck-display-errors-function #'flycheck-pos-tip-error-messages)))))



(use-package auto-complete
  :defer t
  :diminish auto-complete-mode " ⓐ"
  :init
  (progn
    (add-hook 'after-init-hook (lambda () (require 'auto-complete)))
    (setq-default ac-expand-on-auto-complete nil)
    (setq-default ac-dwim nil) ; To get pop-ups with docs even if a word is uniquely completed

    ;;----------------------------------------------------------------------------
    ;; Use Emacs' built-in TAB completion hooks to trigger AC (Emacs >= 23.2)
    ;;----------------------------------------------------------------------------
    (setq tab-always-indent 'complete)  ;; use 't when auto-complete is disabled
    (setq completion-cycle-threshold 5)

    ;; TODO: find solution for php, haskell and other modes where TAB always does something

    (setq c-tab-always-indent nil
          c-insert-tab-function 'indent-for-tab-command)

    (defun set-auto-complete-as-completion-at-point-function ()
      (setq completion-at-point-functions
            (cons 'auto-complete-at-point
                  (remove 'auto-complete-at-point completion-at-point-functions))))
    (add-hook 'auto-complete-mode-hook 'set-auto-complete-as-completion-at-point-function)

    (set-default 'ac-sources
                 '(ac-source-imenu
                   ac-source-words-in-buffer
                   ac-source-words-in-same-mode-buffers
                   ac-source-words-in-all-buffer
                   ac-source-dictionary)))

  :config
  (progn
    (require 'auto-complete-config)
    (global-auto-complete-mode t)
    (add-to-list 'completion-styles 'initials t)
    ;; Stop completion-at-point from popping up completion buffers so eagerly
    ;; hook AC into completion-at-point
    (defun auto-complete-at-point ()
      (when (and (not (minibufferp))
                 (fboundp 'auto-complete-mode)
                 auto-complete-mode)
        #'auto-complete))

    (dolist (mode '(log-edit-mode
                    org-mode
                    text-mode
                    git-commit-mode
                    sass-mode
                    yaml-mode
                    csv-mode
                    espresso-mode
                    haskell-mode
                    html-mode
                    nxml-mode
                    sh-mode
                    smarty-mode
                    clojure-mode
                    lisp-mode
                    markdown-mode
                    tuareg-mode
                    js2-mode
                    css-mode
                    less-css-mode
                    sql-mode
                    sql-interactive-mode
                    inferior-emacs-lisp-mode))
      (add-to-list 'ac-modes mode))))

;; Exclude very large buffers from dabbrev
(defun dabbrev-friend-buffer (other-buffer)
  (< (buffer-size other-buffer) (* 1 1024 1024)))
(setq dabbrev-friend-buffer-function 'dabbrev-friend-buffer)



;; (use-package company
;;   :diminish company-mode " ⓐ"
;;   :commands (company-mode)
;;   :init
;;   (progn
;;     (setq company-idle-delay 0.5
;;           company-tooltip-limit 10
;;           company-minimum-prefix-length 2
;;           company-tooltip-flip-when-above t))
;;   :config
;;   (progn
;;     (setq company-minimum-prefix-length 2)
;;     (global-company-mode)))

;; (use-package smartparens
;;   :diminish smartparens-mode " ⓟ"
;;   :init
;;   (progn
;;     (setq sp-show-pair-delay 0
;;           sp-show-pair-from-inside nil
;;           sp-base-key-bindings 'paredit
;;           sp-autoskip-closing-pair 'always
;;           sp-hybrid-kill-entire-symbol nil
;;           sp-cancel-autoskip-on-backward-movement nil))
;;   :config
;;   (progn
;;     (require 'smartparens-config)
;;     (sp-use-paredit-bindings)
;;     (add-hook 'emacs-lisp-mode-hook #'smartparens-strict-mode)))

(use-package paredit
  :defer t
  :diminish paredit-mode " ⓟ"
  :init
  (autoload 'enable-paredit-mode "paredit")

  :config
  (progn
    (suspend-mode-during-cua-rect-selection 'paredit-mode)

    (defun conditionally-enable-paredit-mode ()
      "Enable paredit during lisp-related minibuffer commands."
      (if (memq this-command paredit-minibuffer-commands)
          (enable-paredit-mode)))
    (add-hook 'minibuffer-setup-hook 'conditionally-enable-paredit-mode)

    (defvar paredit-minibuffer-commands '(eval-expression
                                          pp-eval-expression
                                          eval-expression-with-eldoc
                                          ibuffer-do-eval
                                          ibuffer-do-view-and-eval)
      "Interactive commands for which paredit should be enabled in the minibuffer.")


    (defun forward-transpose-sexps ()
      (interactive)
      (paredit-forward)
      (transpose-sexps 1)
      (paredit-backward))

    (defun backward-transpose-sexps ()
      (interactive)
      (transpose-sexps 1)
      (paredit-backward)
      (paredit-backward))))



(use-package projectile
  :commands (projectile-ag
             projectile-compile-project
             projectile-dired
             projectile-grep
             projectile-find-dir
             projectile-find-file
             projectile-find-tag
             projectile-find-test-file
             projectile-invalidate-cache
             projectile-kill-buffers
             projectile-multi-occur
             projectile-project-root
             projectile-recentf
             projectile-regenerate-tags
             projectile-replace
             projectile-run-async-shell-command-in-root
             projectile-run-shell-command-in-root
             projectile-switch-project
             projectile-switch-to-buffer
             projectile-vc)
  :init
  (progn
    (setq-default projectile-enable-caching t)
    (setq projectile-sort-order 'recentf)
    (setq projectile-cache-file (concat seartipy-cache-directory
                                        "projectile.cache"))
    (setq projectile-known-projects-file (concat seartipy-cache-directory
                                                 "projectile-bookmarks.eld"))
    (evil-leader/set-key
      "pa" 'projectile-find-other-file
      "pb" 'projectile-switch-to-buffer
      "pc" 'projectile-compile-project
      "pd" 'projectile-find-dir
      "pD" 'projectile-dired
      "pe" 'projectile-recentf
      "pf" 'projectile-find-file
      "pg" 'projectile-find-file-dwim
      "pF" 'projectile-find-file-in-known-projects
      "pi" 'projectile-invalidate-cache
      "pI" 'projectile-ibuffer
      "pj" 'projectile-find-tag
      "pk" 'projectile-kill-buffers
      "pl" 'projectile-find-file-in-directory
      "pm" 'projectile-commander
      "po" 'projectile-multi-occur
      "pp" 'projectile-switch-project
      "pP" 'projectile-test-project
      "pr" 'projectile-replace
      "pR" 'projectile-regenerate-tags
      "psg" 'projectile-grep
      "pss" 'projectile-ag
      "pS" 'projectile-save-project-buffers
      "pt" 'projectile-toggle-between-implementation-and-test
      "pT" 'projectile-find-test-file
      "pu" 'projectile-run-project
      "pv" 'projectile-vc
      "pz" 'projectile-cache-current-file
      "p4a" 'projectile-find-other-file-other-window
      "p4b" 'projectile-switch-to-buffer-other-window
      "p4 C-o" 'projectile-display-buffer
      "p4d" 'projectile-find-dir-other-window
      "p4f" 'projectile-find-file-other-window
      "p4g" 'projectile-find-file-dwim-other-window
      "p4t" 'projectile-find-implementation-or-test-other-window
      "p!" 'projectile-run-shell-command-in-root
      "p&" 'projectile-run-async-shell-command-in-root
      "p ESC" 'projectile-project-buffers-other-buffer))
  :config
  (projectile-global-mode))



;; (use-package back-button
;;   :diminish back-button-mode
;;   :init
;;   (progn
;;     (back-button-mode)))

(use-package neotree
  :bind ("<f8>" . neotree-toggle))

(use-package yasnippet
  :commands (yas-reload-all yas-minor-mode yas-global-mode)
  :init
  (setq yas-installed-snippets-dir (concat user-emacs-directory "snippets"))
  (add-hook 'prog-mode-hook
            '(lambda ()
               (yas-minor-mode)
               (diminish 'yas-minor-mode " ⓨ"))))

(use-package zeal-at-point
  :init
  :bind ("\C-cd" . zeal-at-point))

(provide 'init-ide)
