(use-package json-mode
  :mode "\\.json\\'")

(use-package web-mode
  :mode "\\.jsx\\'"
  :init
  (progn
    (setq-default flycheck-disabled-checkers
                  (append flycheck-disabled-checkers
                          '(javascript-jshint)))

    (flycheck-add-mode 'javascript-eslint 'web-mode)

    (defun my-web-mode-hook ()
      (setq web-mode-markup-indent-offset 2)
      (setq web-mode-css-indent-offset 2)
      (setq web-mode-code-indent-offset 2))
    (add-hook 'web-mode-hook  'my-web-mode-hook)))

(use-package js2-mode
  :mode "\\.js\\'"
  :init
  (progn
    ;; Disable js2 mode's syntax error highlighting by default...
    (setq-default js2-mode-show-parse-errors nil
                  js2-mode-show-strict-warnings nil
                  js2-strict-missing-semi-warning nil
                  js2-basic-offset 2)

    (add-hook 'js2-mode-hook
              (lambda ()
                (setq mode-name "JS")
                (js2-imenu-extras-setup)
                (js2-mode-hide-warnings-and-errors)
                (rainbow-delimiters-mode)))))

(use-package coffee-mode
  :mode "\\.coffee\\'"
  :config
  (progn
    ;;coffeescript settings
    (custom-set-variables '(coffee-tab-width 2))
    (setq coffee-args-compile '("-c" "-m")) ;; generating sourcemap
    (add-hook 'coffee-after-compile-hook 'sourcemap-goto-corresponding-point)

    ;; If you want to remove sourcemap file after jumping corresponding point
    (defun my/coffee-after-compile-hook (props)
      (sourcemap-goto-corresponding-point props)
      (delete-file (plist-get props :sourcemap)))
    (add-hook 'coffee-after-compile-hook 'my/coffee-after-compile-hook)))

(use-package tern
  :commands (tern-mode)
  :load-path "~/seartipy/vendors/tern/emacs"
  :init
  (add-hook 'js2-mode-hook (lambda () (tern-mode)))
  :config
  (after-load 'tern
    (use-package tern-auto-complete)
    (tern-ac-setup)))

;;run javascript from within emacs using skewer-mode.
(use-package skewer-mode
  :defer t
  :diminish skewer-mode
  :init
  (progn
    (add-hook 'js2-mode-hook 'skewer-mode)
    (add-hook 'css-mode-hook 'skewer-css-mode)
    (add-hook 'html-mode-hook 'skewer-html-mode)))

;; refactoring support for javascript
(use-package js2-refactor
  :defer t
  :init
  (add-hook 'js2-mode-hook (lambda () (require 'js2-refactor)))
  :config
  (js2r-add-keybindings-with-prefix "C-c C-m"))

;;emmet-mode setup for emmet support in html/css files
(use-package emmet-mode
  :diminish emmet-mode
  :init
  (progn
    (add-hook 'sgml-mode-hook 'emmet-mode)
    (add-hook 'css-mode-hook  'emmet-mode)
    (add-hook 'emmet-mode-hook (lambda () (setq emmet-indentation 4)))
    (setq emmet-move-cursor-between-quotes t)))

;; (use-package web-beautify
;;   :init
;;   (progn
;;     (after-load 'js2-mode
;;       (bind-key "C-c C-b" 'web-beautify-js js2-mode-map))
;;     (after-load 'json-mode
;;       (bind-key "C-c C-b" 'web-beautify-js json-mode-map))

;;     (after-load 'sgml-mode
;;       (bind-key "C-c C-b" 'web-beautify-html html-mode-map))

;;     (after-load 'css-mode
;;       (bind-key "C-c C-b" 'web-beautify-css) css-mode-map)))

(use-package tidy
  :init
  (add-hook 'html-mode-hook (lambda () (tidy-build-menu html-mode-map))))

(use-package tagedit
  :diminish tagedit-mode
  :init
  (after-load 'sgml-mode
    (tagedit-add-paredit-like-keybindings)
    (add-hook 'sgml-mode-hook (lambda () (tagedit-mode)))))

;;; Colourise CSS colour literals
(use-package rainbow-mode
  :diminish rainbow-mode
  :init
  (dolist (hook '(css-mode-hook html-mode-hook sass-mode-hook))
    (add-hook hook 'rainbow-mode)))

;;; SASS and SCSS
(use-package sass-mode
  :defer t)
(use-package scss-mode
  :defer t
  :init
  (setq-default scss-compile-at-save nil))

;;; LESS
(use-package less-css-mode)
(when (featurep 'js2-mode)
  (use-package skewer-less))

;;; Use eldoc for syntax hints
(use-package css-eldoc
  :init
  (autoload 'turn-on-css-eldoc "css-eldoc")
  (add-hook 'css-mode-hook 'turn-on-css-eldoc))

(provide 'init-web)
