(use-package diff-hl
  :defer t
  :init
  (progn
    (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
    (add-hook 'vc-dir-mode-hook 'turn-on-diff-hl-mode)
    (after-load 'dired
      (add-hook 'dired-mode-hook 'diff-hl-dired-mode))))

(use-package git-blame :defer t)
(use-package gitignore-mode :defer t)
(use-package gitconfig-mode :defer t)
(use-package git-messenger :defer t)

(use-package git-timemachine
  :commands (git-timemachine))

(use-package fullframe)

(use-package magit
  :bind (("C-x g" . magit-status)
         ("C-x M-g" . magit-dispatch-popup))
  :init
  (progn
    (setq-default
     magit-process-popup-time 10
     magit-diff-refine-hunk t
     magit-completing-read-function 'magit-ido-completing-read)
    (after-load 'magit
      (define-key magit-status-mode-map (kbd "C-M-<up>") 'magit-section-up))
    (after-load 'magit
      (fullframe magit-status magit-mode-quit-window))))

(use-package git-commit
  :defer t
  :init
  (progn
    (add-hook 'git-commit-mode-hook 'goto-address-mode)
    (when *is-a-mac*
      (after-load 'magit
        (add-hook 'magit-mode-hook (lambda () (local-unset-key [(meta h)])))))))


;; Convenient binding for vc-git-grep
(bind-key "C-x v f" 'vc-git-grep)

(use-package git-messenger
  :bind ("C-x v p" . git-messenger:popup-message))

(use-package gist
  :commands (gist-list))

(use-package yagist
  :commands (yagist-list))

(use-package github-browse-file
  :commands (github-browse-file))

(use-package bug-reference-github
  :defer t
  :init
  (add-hook 'prog-mode-hook 'bug-reference-prog-mode))

(use-package github-clone
  :commands (github-clone))

(provide 'init-vc)
