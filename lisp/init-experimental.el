(use-package ace-isearch)
(require 'ace-isearch)
(define-key isearch-mode-map (kbd "C-'") 'ace-isearch-jump-during-isearch)

(provide 'init-experimental)
