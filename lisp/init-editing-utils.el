(bind-key "RET" 'newline-and-indent)

(defun newline-at-end-of-line ()
  "Move to end of line, enter a newline, and reindent."
  (interactive)
  (move-end-of-line 1)
  (newline-and-indent))

(use-package undo-tree
  :diminish undo-tree-mode
  :config
  (global-undo-tree-mode))

(use-package browse-kill-ring
  :commands (browse-kill-ring)
  :init
  (setq browse-kill-ring-separator "\f"))

;; Don't disable narrowing commands
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

(use-package expand-region
  :bind ("C-=" . er/expand-region))

(use-package ace-jump-mode
  :bind (("C-'" . ace-jump-mode)
         ("C-c <SPC>" . ace-jump-mode)
         ("C-c C-<SPC>" . ace-jump-line-mode)
         ("C-c M-<SPC>" . ace-jump-char-mode)))

(use-package avy
  :commands (avy-goto-char avy-goto-char-2 avy-goto-word-1 avy-goto-word-2)
  :init
  (progn
    (setq avy-keys (number-sequence ?a ?z)
          avy-style 'pre
          avy-all-windows nil
          avy-background t)

    (evil-leader/set-key
      "C-SPC" 'avy-goto-word-0
      "SPC" 'avy-goto-word-1
      "M-SPC" 'avy-goto-char
      "S-SPC" 'avy-goto-char-2
      "l" 'avy-goto-line))
  :config
  (evil-leader/set-key "`" 'avy-pop-mark))

(use-package multiple-cursors
  :bind (("C-<" . mc/mark-previous-like-this)
         ("C->" . mc/mark-next-like-this)
         ("C-c +" . mc/mark-next-like-this)
         ("C-c C-<" . mc/mark-all-like-this)
         ;; From active region to multiple cursors:
         ("C-c c r" . set-rectangular-region-anchor)
         ("C-c c c" . mc/edit-lines)
         ("C-c c e" . mc/edit-ends-of-lines)
         ("C-c c a" . mc/edit-beginnings-of-lines)))

(defun kill-back-to-indentation ()
  "Kill from point back to the first non-whitespace character on the line."
  (interactive)
  (let ((prev-pos (point)))
    (back-to-indentation)
    (kill-region (point) prev-pos)))

(use-package move-dup
  :bind (("M-S-<up>" . md/move-lines-up)
         ("M-S-<down>" . md/move-lines-down)
         ("C-c C-p" . md/duplicate-down)
         ("C-c C-P" . md/duplicate-up)))

;; Fix backward-up-list to understand quotes, see http://bit.ly/h7mdIL
(defun backward-up-sexp (arg)
  "Jump up to the start of the ARG'th enclosing sexp."
  (interactive "p")
  (let ((ppss (syntax-ppss)))
    (cond ((elt ppss 3)
           (goto-char (elt ppss 8))
           (backward-up-sexp (1- arg)))
          ((backward-up-list arg)))))

;; Cut/copy the current line if no region is active
;; (use-package whole-line-or-region)
;; (whole-line-or-region-mode t)
;; (diminish 'whole-line-or-region-mode)
;; (make-variable-buffer-local 'whole-line-or-region-mode)

(defun suspend-mode-during-cua-rect-selection (mode-name)
  "Add an advice to suspend `MODE-NAME' while selecting a CUA rectangle."
  (let ((flagvar (intern (format "%s-was-active-before-cua-rectangle" mode-name)))
        (advice-name (intern (format "suspend-%s" mode-name))))
    (eval-after-load 'cua-rect
      `(progn
         (defvar ,flagvar nil)
         (make-variable-buffer-local ',flagvar)
         (defadvice cua--activate-rectangle (after ,advice-name activate)
           (setq ,flagvar (and (boundp ',mode-name) ,mode-name))
           (when ,flagvar
             (,mode-name 0)))
         (defadvice cua--deactivate-rectangle (after ,advice-name activate)
           (when ,flagvar
             (,mode-name 1)))))))

(suspend-mode-during-cua-rect-selection 'whole-line-or-region-mode)

(use-package highlight-escape-sequences
  :config
  (hes-mode))

(setq auto-save-default nil)

(use-package easy-kill
  :init
  (bind-key [remap kill-ring-save] 'easy-kill))

(use-package indent-guide
  :diminish indent-guide-mode " ⓘ"
  :config
  (indent-guide-mode))

(use-package open-junk-file
  :commands (open-junk-file)
  :init
  (evil-leader/set-key "fJ" 'open-junk-file)
  (setq open-junk-file-directory (concat seartipy-cache-directory "junk/")))

(provide 'init-editing-utils)
