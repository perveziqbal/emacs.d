(use-package scala-mode2
  :bind ("<backtab>" . scala-indent:indent-with-reluctant-strategy)
  :init
  (setq scala-indent:align-parameters t
        scala-indent:align-forms t
        scala-indent:indent-value-expression t)
  :config
  (setq scala-indent:default-run-on-strategy scala-indent:operator-strategy))

(use-package sbt-mode
  :defer t
  :init
  (progn
    (add-hook 'scala-mode-hook
              (lambda () (require 'sbt-mode)))
    (add-hook 'sbt-mode-hook
              '(lambda ()
                 (local-set-key (kbd "C-a") 'comint-bol)
                 (local-set-key (kbd "M-RET") 'comint-accumulate)
                 ))
    (add-hook 'scala-mode-hook
              '(lambda ()
                 (local-set-key (kbd "M-.") 'sbt-find-definitions)
                 (local-set-key (kbd "C-x '") 'sbt-run-previous-command)))))

(use-package ensime
  :commands (ensime-mode)
  :init
  (progn
    (setq ensime-sem-high-enabled-p nil)
    (add-hook 'ensime-mode-hook (lambda ()
                                  ;; (setq-local eldoc-documentation-function
                                  ;;             #'(lambda ()
                                  ;;                 (when (ensime-connected-p)
                                  ;;                   (let ((err (ensime-print-errors-at-point)))
                                  ;;                     (or (and err (not (string= err "")) err)
                                  ;;                         (ensime-print-type-at-point))))))
                                  (auto-complete-mode -1)
                                  (eldoc-mode +1))))

  ;; Don't use scala checker if ensime mode is active, since it provides
  ;; better error checking.
  (after-load 'flycheck
    (add-hook 'ensime-mode-hook (lambda ()
                                  (flycheck-mode -1))))
  :config
  (setq user-emacs-ensime-directory ".cache/ensime"))

(evil-leader/set-key-for-mode 'scala-mode
  "m/"     'ensime-search

  "mbc"     'ensime-sbt-do-compile
  "mbC"     'ensime-sbt-do-clean
  "mbi"     'ensime-sbt-switch¯
  "mbp"     'ensime-sbt-do-package
  "mbr"     'ensime-sbt-do-run

  "mct"     'ensime-typecheck-current-file
  "mcT"     'ensime-typecheck-all

  "mdA"     'ensime-db-attach
  "mdb"     'ensime-db-set-break
  "mdB"     'ensime-db-clear-break
  "mdC"     'ensime-db-clear-all-breaks
  "mdc"     'ensime-db-continue
  "mdd"     'ensime-db-start
  "mdi"     'ensime-db-inspect-value-at-point
  "mdl"     'ensime-db-list-locals
  "mdn"     'ensime-db-next
  "mdo"     'ensime-db-step-out
  "mdq"     'ensime-db-quit
  "mdr"     'ensime-db-run
  "mds"     'ensime-db-step
  "mdt"     'ensime-db-backtrace

  "mee"     'ensime-print-errors-at-point
  "mel"     'ensime-show-all-errors-and-warnings
  "mes"     'ensime-stacktrace-switch

  "mgg"     'ensime-edit-definition
  "mgp"     'ensime-pop-find-definition-stack
  "mgi"     'ensime-goto-impl
  "mgt"     'ensime-goto-test

  "mhh"     'ensime-show-doc-for-symbol-at-point
  "mhu"     'ensime-show-uses-of-symbol-at-point
  "mht"     'ensime-print-type-at-point

  "mii"     'ensime-inspect-type-at-point
  "miI"     'ensime-inspect-type-at-point-other-frame
  "mip"     'ensime-inspect-project-package

  "mnF"     'ensime-reload-open-files
  "mns"     'ensime
  "mnS"     'ensime-gen-and-restart

  "mrd"     'ensime-refactor-inline-local
  "mrD"     'ensime-undo-peek
  "mrf"     'ensime-format-source
  "mri"     'ensime-refactor-organize-imports
  "mrm"     'ensime-refactor-extract-method
  "mrr"     'ensime-refactor-rename
  "mrt"     'ensime-import-type-at-point
  "mrv"     'ensime-refactor-extract-local

  "mta"     'ensime-sbt-do-test-dwim
  "mtr"     'ensime-sbt-do-test-quick-dwim
  "mtt"     'ensime-sbt-do-test-only-dwim

  "msa"     'ensime-inf-load-file
  "msb"     'ensime-inf-eval-buffer
  "msB"     'ensime-inf-eval-buffer-switch
  "msi"     'ensime-inf-switch
  "msr"     'ensime-inf-eval-region
  "msR"     'ensime-inf-eval-region-switch

  "mz"      'ensime-expand-selection-command
  )

(provide 'init-scala)
