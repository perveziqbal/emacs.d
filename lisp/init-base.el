(use-package multi-term
  :bind (("<f5>" . multi-term-dedicated-toggle))
  :commands (multi-term)
  :init
  (custom-set-variables '(multi-term-dedicated-select-after-open-p t))
  (setq multi-term-scroll-to-bottom-on-output "others")
  (setq multi-term-scroll-show-maximum-output +1)
  :config
  (progn
    (add-to-list 'term-bind-key-alist '("M-DEL" . term-send-backward-kill-word))
    (add-to-list 'term-bind-key-alist '("M-d" . term-send-forward-kill-word))))

(setq-default diredp-hide-details-initially-flag nil
              dired-dwim-target t)

;; Prefer g-prefixed coreutils version of standard utilities when available
(let ((gls (executable-find "gls")))
  (when gls (setq insert-directory-program gls)))

(after-load 'dired
  (require 'dired+)
  (require 'dired-sort)
  (when (fboundp 'global-dired-hide-details-mode)
    (global-dired-hide-details-mode -1))
  (setq dired-recursive-deletes 'top)
  (define-key dired-mode-map [mouse-2] 'dired-find-file))

(defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
  (flet ((process-list ())) ad-do-it))

(use-package unfill
  :commands (unfill-region unfill-paragraph toggle-fill-unfill))

(use-package editorconfig
  :init
  (add-hook 'after-init-hook (lambda () (require 'editorconfig))))

(use-package discover-my-major
  :commands (discover-my-mode discover-my-major))

(provide 'init-base)
