(setq-default grep-highlight-matches t
              grep-scroll-output t)

(when *is-a-mac*
  (setq-default locate-command "mdfind"))

(use-package ag
  :commands (ag ag-files ag-regexp ag-project ag-dired)
  :bind ("M-?" . ag-project)
  :config (setq ag-highlight-search t
                ag-reuse-buffers t))

(use-package wgrep
  :commands (wgrep-ag-setup)
  :init
  (add-hook 'ag-mode-hook 'wgrep-ag-setup)
  :config
  (use-package wgrep-ag))

(use-package helm-swoop
  :commands (helm-swoop)
  :init
  (evil-leader/set-key
    "ss"    'helm-swoop
    "sS" 'helm-multi-swoop-all))

(use-package helm-ag
  :commands (helm-ag)
  :init
  (custom-set-variables
   '(helm-ag-use-aginore t)
   '(helm-ag-instert-at-point 'symbol)))

(use-package anzu
  :diminish anzu-mode
  :config
  (global-anzu-mode))

(use-package helm-ag
  :defer t
  :init
  (progn
    (evil-leader/set-key
      "sa" 'helm-ag
      "sA" 'helm-do-ag

      "sf" 'helm-ag-this-file
      "sF" 'helm-do-ag-this-file

      "sb" 'helm-ag-buffers
      "sB" 'helm-do-ag-buffers

      "sp" 'helm-ag-project-root
      "sP" 'helm-do-ag-project-root
      "s`" 'helm-ag-pop-stack)))

(provide 'init-search)
