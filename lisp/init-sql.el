(use-package sql-indent
  :defer t
  :init
  (after-load 'sql
    (require 'sql-indent)))

(defun pop-to-sqli-buffer ()
  "Switch to the corresponding sqli buffer."
  (interactive)
  (if sql-buffer
      (progn
        (pop-to-buffer sql-buffer)
        (goto-char (point-max)))
    (sql-set-sqli-buffer)
    (when sql-buffer
      (pop-to-sqli-buffer))))

(after-load 'sql
  (bind-key "C-c C-z" 'pop-to-sqli-buffer sql-mode-map)
  (add-hook 'sql-interactive-mode-hook 'never-indent)
  (when (package-installed-p 'dash-at-point)
    (defun maybe-set-dash-db-docset ()
      (when (eq sql-product 'postgres)
        (setq (make-local-variable 'dash-at-point-docset) "psql")))

    (add-hook 'sql-mode-hook 'maybe-set-dash-db-docset)
    (add-hook 'sql-interactive-mode-hook 'maybe-set-dash-db-docset)
    (defadvice sql-set-product (after set-dash-docset activate)
      (maybe-set-dash-db-docset))))

(setq-default sql-input-ring-file-name
              (expand-file-name ".sqli_history" user-emacs-directory))

;; See my answer to https://emacs.stackexchange.com/questions/657/why-do-sql-mode-and-sql-interactive-mode-not-highlight-strings-the-same-way/673
(defun font-lock-everything-in-sql-interactive-mode ()
  (unless (eq 'oracle sql-product)
    (sql-product-font-lock nil nil)))
(add-hook 'sql-interactive-mode-hook 'font-lock-everything-in-sql-interactive-mode)

(provide 'init-sql)
