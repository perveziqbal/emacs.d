(use-package ruby-mode
  :defer t
  :init
  (progn
    (add-auto-mode 'ruby-mode
                   "Rakefile\\'" "\\.rake\\'" "\\.rxml\\'"
                   "\\.rjs\\'" "\\.irbrc\\'" "\\.pryrc\\'" "\\.builder\\'" "\\.ru\\'"
                   "\\.gemspec\\'" "Gemfile\\'" "Kirkfile\\'")
    (setq ruby-use-encoding-map nil)
    (after-load 'ruby-mode
      (define-key ruby-mode-map (kbd "TAB") 'indent-for-tab-command)
      ;; Stupidly the non-bundled ruby-mode isn't a derived mode of
      ;; prog-mode: we run the latter's hooks anyway in that case.
      (add-hook 'ruby-mode-hook
                (lambda ()
                  (unless (derived-mode-p 'prog-mode)
                    (run-hooks 'prog-mode-hook)))))
    (add-hook 'ruby-mode-hook 'subword-mode)))

(use-package ruby-hash-syntax)

(use-package inf-ruby
  :init
  (after-load 'inf-ruby
    (define-key inf-ruby-mode-map (kbd "TAB") 'auto-complete)))

(use-package ac-inf-ruby
  :init
  (after-load 'auto-complete
    (add-to-list 'ac-modes 'inf-ruby-mode))
  (add-hook 'inf-ruby-mode-hook 'ac-inf-ruby-enable))

(use-package ruby-compilation
  :init
  (after-load 'ruby-compilation
    (defalias 'rake 'ruby-compilation-rake)))

(use-package robe
  :init
  (after-load 'ruby-mode
    (add-hook 'ruby-mode-hook 'robe-mode)

    (defun maybe-enable-robe-ac ()
      "Enable/disable robe auto-complete source as necessary."
      (if robe-mode
          (progn
            (add-hook 'ac-sources 'ac-source-robe nil t)
            (set-auto-complete-as-completion-at-point-function))
        (remove-hook 'ac-sources 'ac-source-robe)))

    (after-load 'robe
      (add-hook 'robe-mode-hook 'maybe-enable-robe-ac))))

(use-package yari
  :init
  (defalias 'ri 'yari))

(use-package yaml-mode)

(provide 'init-ruby-mode)
