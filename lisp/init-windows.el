;; Navigate window layouts with "C-c <left>" and "C-c <right>"
(winner-mode)

;; When splitting window, show (other-buffer) in the new window
(defun split-window-func-with-other-buffer (split-function)
  (lexical-let ((s-f split-function))
    (lambda ()
      (interactive)
      (funcall s-f)
      (set-window-buffer (next-window) (other-buffer)))))

;; rearrange split windows
(defun split-window-horizontally-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-horizontally))))

(defun split-window-vertically-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-vertically))))

(windmove-default-keybindings)

(use-package ace-window
  :commands (ace-window ace-swap-window ace-delete-window)
  :init
  (progn
    (evil-leader/set-key
      "bM"  'ace-swap-window
      "wC"  'ace-delete-window
      "w <SPC>"  'ace-window))
  :config
  (progn
    (setq aw-dispatch-always t)))

(use-package golden-ratio
  :diminish " ⓖ"
  :commands (golden-ratio-mode))

;; window functions

(defun spacemacs/switch-to-minibuffer-window ()
  "switch to minibuffer window (if active)"
  (interactive)
  (when (active-minibuffer-window)
    (select-window (active-minibuffer-window))))

(defun split-window-below-and-focus ()
  "Split the window vertically and focus the new window."
  (interactive)
  (split-window-below)
  (windmove-down))

(defun split-window-right-and-focus ()
  "Split the window horizontally and focus the new window."
  (interactive)
  (split-window-right)
  (windmove-right))

(defun layout-triple-columns ()
  " Set the layout to triple columns. "
  (interactive)
  (delete-other-windows)
  (split-window-right)
  (split-window-right)
  (balance-windows))

(defun layout-double-columns ()
  " Set the layout to double columns. "
  (interactive)
  (delete-other-windows)
  (split-window-right))

(defun layout-triple-rows ()
  " Set the layout to triple columns. "
  (interactive)
  (delete-other-windows)
  (split-window-below)
  (split-window-below)
  (balance-windows))

(defun layout-double-rows ()
  " Set the layout to double columns. "
  (interactive)
  (delete-other-windows)
  (split-window-below))

(evil-leader/set-key
  "w@"  'layout-double-rows
  "w#"  'layout-triple-rows
  "w2"  'layout-double-columns
  "w3"  'layout-triple-columns

  "wb"  'switch-to-minibuffer-window
  "wc"  'delete-window

  "wH"  'evil-window-move-far-left
  "wh"  'evil-window-left
  "wJ"  'evil-window-move-very-bottom
  "wj"  'evil-window-down
  "wK"  'evil-window-move-very-top
  "wk"  'evil-window-up
  "wL"  'evil-window-move-far-right
  "wl"  'evil-window-right

  "wo"  'other-frame
  "wu"  'winner-undo
  "wv"  'split-window-right

  "ws"  'split-window-below
  "wS"  'split-window-below-and-focus
  "w-"  'split-window-below
  "wU"  'winner-redo
  "wV"  'split-window-right-and-focus
  "w/"  'split-window-right

  "ww"  'other-window
  "w="  'balance-windows

  "w_" 'split-window-horizontally-instead
  "w|" 'split-window-vertically-instead)

(use-package window-numbering
  :init
  (require 'window-numbering)

  :config
  (progn
    (evil-leader/set-key
      "0" 'select-window-0
      "1" 'select-window-1
      "2" 'select-window-2
      "3" 'select-window-3
      "4" 'select-window-4
      "5" 'select-window-5
      "6" 'select-window-6
      "7" 'select-window-7
      "8" 'select-window-8
      "9" 'select-window-9)
    (window-numbering-mode 1)))

(provide 'init-windows)
