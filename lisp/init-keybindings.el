;;;; editing

(autoload 'zap-up-to-char "misc" "Kill up to, but not including ARGth occurrence of CHAR.")
(bind-key "M-Z" 'zap-up-to-char)

(bind-key "S-<return>" 'newline-at-end-of-line)
(bind-key "C-S-o" 'newline-at-end-of-line)

;; Vimmy alternatives to M-^ and C-u M-^
(bind-key "C-c j" 'join-line)
(bind-key "C-c J" (lambda () (interactive) (join-line 1)))

(bind-key "C-." 'set-mark-command)
(bind-key "C-x C-." 'pop-global-mark)

(bind-key "C-M-<backspace>" 'kill-back-to-indentation)

(bind-key [remap backward-up-list] 'backward-up-sexp) ; C-M-u, C-M-up

(bind-key "M-/" 'hippie-expand)



;;;; windows

(bind-key "C-x C-2" (split-window-func-with-other-buffer 'split-window-vertically))
(bind-key "C-x C-3" (split-window-func-with-other-buffer 'split-window-horizontally))
(bind-key "\C-x|" 'split-window-horizontally-instead)
(bind-key "\C-x_" 'split-window-vertically-instead)
(bind-key (kbd "C-x o") 'ace-window)



;;;; buffer

(bind-key "C-c b" 'switch-to-previous-buffer)
(evil-leader/set-key "bb" 'switch-to-previous-buffer)
(bind-key "C-x C-b" 'ibuffer)



(key-chord-define-global "qq" 'ace-jump-mode)
(key-chord-define-global "zz" 'zap-up-to-char)

(global-set-key (kbd "C-x p") 'proced)

;; (autoload 'zap-up-to-char "misc")
;; (bind-key "M-Z" 'zap-up-to-char)

;; (bind-key "C-." 'set-mark-command)
;; (bind-key "C-x C-." 'pop-global-mark)

;; (bind-key "C-c C-p" 'md/duplicate-down)
;; (bind-key "C-c C-P" 'md/duplicate-up)

;; (bind-key "C-c b" 'switch-to-previous-buffer)
;; (bind-key "C-c s" 'cycle-spacing)
;; (bind-key "C-c z" 'zap-up-to-char)
;; ;; Vimmy alternatives to M-^ and C-u M-^
;; (bind-key "C-c j"  'join-line)
;; (bind-key "C-c J" (lambda () (interactive) (join-line 1)))
;; (bind-key "C-c SPC" 'ace-jump-mode)
;; (bind-key "C-c b" 'switch-to-previous-buffer)
;; (bind-key "C-c f" 'find-file)
;; (bind-key "C-c F" 'find-file-other-window)
;; (bind-key "C-c o" 'find-file-other-window)
;; (bind-key "C-c q" 'save-buffers-kill-terminal)

;; (bind-key "C-c h" 'split-window-right)
;; (bind-key "C-c v" 'split-window-below)
;; (bind-key "C-c d" 'delete-window)

(provide 'init-keybindings)
